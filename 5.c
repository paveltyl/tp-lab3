#include <stdio.h>
#include <ctype.h>

int main() {
    FILE *in = fopen("in", "rt");
    FILE *out = fopen("out", "wt");

    int c;
    while ((c = fgetc(in)) != EOF) { //reading whole file by characters till EOF
        if (isdigit(c)) {
            c = '!'; //all digits changet to '!'
        }
        fputc(c, out);
    }

    return 0;
}